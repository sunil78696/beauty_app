import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { HomeComponent } from './screens/home/home.component';
import { ServicesComponent } from './screens/services/services.component';
import { AboutComponent } from './screens/about/about.component';
import { ContactComponent } from './screens/contact/contact.component';
import { CartComponent } from './screens/cart/cart.component';
import { CheckoutComponent } from './screens/checkout/checkout.component';
import { FaqComponent } from './screens/faq/faq.component';
import { BeautitionInfoComponent } from './screens/beautition-info/beautition-info.component';
import { ModalComponent } from './shared/modal/modal.component';
import { PageNotFoundComponent } from './screens/page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ServicesComponent,
    AboutComponent,
    ContactComponent,
    CartComponent,
    CheckoutComponent,
    FaqComponent,
    BeautitionInfoComponent,
    ModalComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [{provide:APP_BASE_HREF,useValue : ""}],
  bootstrap: [AppComponent]
})
export class AppModule { }
